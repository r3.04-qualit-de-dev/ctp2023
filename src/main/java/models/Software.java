package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The main class of the project.
 */
public class Software {
    
    private static final Random random = new Random();

    /**
     * The list of persons that are writen in the software.
     */
    private List<Person> personsList;

    public Software(){
        personsList = new ArrayList<>();
    }

    public Person getPerson(String name){
        for(Person p : personsList){
            if(p.getName().equals(name)) return p;
        }
        return null;
    }

    public boolean addPerson(Person person){
        if(isNameAvailable(person.getName())){
            personsList.add(person);
            return true;
        }
        else return false;
    }

    public boolean isNameAvailable(String name){
        for(Person p : personsList){
            if(p.getName().equals(name)) return false;
        }
        return true;
    }

    public void assignedPersonToGift(Person person, Person personToGift){
        if(person!=personToGift){
            person.setPersonToGift(personToGift);
        }
    }

    public void rollEveryPersonToGift(){
        List<Person> temp = new ArrayList<>();
        List<Person> queue = new ArrayList<>();
        temp.addAll(personsList);

        for(Person p : temp){
            
        }
    }

}
