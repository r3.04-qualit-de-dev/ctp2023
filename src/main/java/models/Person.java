package models;

/**
 * Represent a person.
 */
public class Person {
    
    /**
     * The name of that person.
     */
    private String name = null;

    /**
     * The person who have to get his gift.
     */
    private Person personToGift = null;

    public Person(String name){
        this.name = name;
    }

    /**
     * Set the person who have to get a gift from this person.
     * @param p The person who need a gift
     * @return The person who need a gift
     */
    public Person setPersonToGift(Person p){
        this.personToGift = p;
        return p;
    }

    public String getName(){
        return this.name;
    }

    public Person getPersonToGift(){
        return this.personToGift;
    }

}
