package models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PersonTest {
    
    Person Antoine,Arthur,Ludovic,Intru;

    @BeforeEach
    public void init(){
        Antoine = new Person("Antoine");
        Arthur = new Person("Arthur");
        Ludovic = new Person("Arthur");
        Intru = new Person(null);
    }

    @Test
    public void testGetName(){
        assertEquals(Antoine.getName(), "Antoine");
        assertNotEquals(Arthur.getName(), "Intru");
        assertEquals(Intru.getName(), null);
        assertNotEquals(Ludovic, "Arthur");
    }

    @Test
    public void testGetAndSetPersonToGift(){
        assertEquals(Arthur.getPersonToGift(), null);
        assertEquals(Arthur.setPersonToGift(Antoine), Antoine);
        assertEquals(Arthur.getPersonToGift(), Antoine);
    }

}
