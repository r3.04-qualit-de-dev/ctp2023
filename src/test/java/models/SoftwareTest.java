package models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SoftwareTest {
    
    Software software;
    Person antoine,arthur,ludovic,intru;

    @BeforeEach
    public void init(){
        software = new Software();
        antoine = new Person("antoine");
        arthur = new Person("arthur");
        ludovic = new Person("arthur");
        intru = new Person(null);
    }

    @Test
    public void testAddPerson(){
        assertTrue(software.isNameAvailable(arthur.getName()));
        assertTrue(software.addPerson(arthur));
        assertEquals(software.getPerson(arthur.getName()), arthur);
        assertFalse(software.isNameAvailable(arthur.getName()));
        assertFalse(software.addPerson(intru));
    }

    @Test
    public void testRollEveryPersonToGift(){
        software.addPerson(arthur);
        software.addPerson(antoine);
        software.addPerson(ludovic);
        software.rollEveryPersonToGift();
        assertFalse(software.getPerson(arthur.getName()).getPersonToGift()!=arthur);
        assertFalse(software.getPerson(antoine.getName()).getPersonToGift()!=antoine);
        assertFalse(software.getPerson(ludovic.getName()).getPersonToGift()!=ludovic);
    }

}
